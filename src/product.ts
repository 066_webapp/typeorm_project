import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    // console.log("Inserting a new Product into the database...")
    const productsRepository = AppDataSource.getRepository(Product)
    const product = new Product()
    // product.name = "ข้าวมันไก่"
    // product.price = 50
    // await productsRepository.save(product)
    // console.log("Saved a new product with id: " + product.id)

    // console.log("Loading products from the database...")
    const products = await productsRepository.find()
    console.log("Loaded products: ", products)

    // console.log("Here you can setup and run express / fastify / any other framework.")

    const updatedProduct = await productsRepository.findOneBy({ id:1 })
    console.log("product: ", updatedProduct)
    updatedProduct.price = 60
    await productsRepository.save(updatedProduct)
    console.log("Updated product: ", updatedProduct)

}).catch(error => console.log(error))

// npx ts-node src/product.ts
